import socket

host = '127.0.0.1'
port = 8000

client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client.sendto(bytes('AAABBBCCC','utf-8'), (host, port))
data, addr = client.recvfrom(4096)
print(data.decode('utf-8'))